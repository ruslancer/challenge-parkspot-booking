import {describe, expect, test, jest, beforeEach} from '@jest/globals';
import { BookingRepository } from '../../src/repositories/BookingRepository';
import { prismaMock } from '../singleton'

describe('bookingRepository', () => {
  const repository = new BookingRepository(prismaMock)

  test('isValidBookingPayload', () => {
    [
        {
            data: {
                "startDate": "2023-10-29T18:48:29Z",
                "endDate": "2023-10-30T18:48:29Z",
                "spotId": 3
            },
            result: true
        },
        {
            data: {

            },
            result: false
        },
        {
            data: {
                "startDate": 'invalid date',
                "endDate": "2023-10-30T18:48:29Z",
                "spotId": 3
            },
            result: false
        }
    ].map((item) => {
        const result = repository.isValidBookingPayload(item.data)
      
        expect(result).toBe(item.result);
    })
  });
});

