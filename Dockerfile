FROM node:21

WORKDIR /usr/src/app

COPY . . 
COPY .env-docker .env
RUN npm install
RUN npm run build

EXPOSE 8000

CMD ["node", "--env-file='.env'", "build/src/index.js"]