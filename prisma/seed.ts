import { PrismaClient } from '@prisma/client'
import jsonwebtoken from 'jsonwebtoken'
import { env } from 'process'

const prisma = new PrismaClient()
async function main() {
  for (let i = 0; i < 5; i++) {
    await prisma.spot.create({
      data: {
        name: `spot #${i+1}`
      }
    })
  }


  const user = await prisma.role.create({
    data: {
        'name': 'User'
    }
  })
  const admin = await prisma.role.create({
    data: {
        'name': 'Admin'
    }
  })

  const alice = await prisma.user.create({
    data: {
      email: 'alice@prisma.io',
      fisrtName: 'Alice',
      lastName: 'Doe',
      roleId: user.id,
      token: jsonwebtoken.sign({ email: 'alice@prisma.io' }, env.SECRET ?? '')
    },
  })
  const bob = await prisma.user.create({
    data: {
      email: 'bob@prisma.io',
      fisrtName: 'Bob',
      lastName: 'Doe',
      roleId: admin.id,
      token: jsonwebtoken.sign({ email: 'bob@prisma.io' }, env.SECRET ?? '')
    },
  })
  console.log({ alice, bob })
}
main()
  .then(async () => {
    await prisma.$disconnect()
  })
  .catch(async (e) => {
    console.error(e)
    await prisma.$disconnect()
    process.exit(1)
  })