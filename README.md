# How to run

First start docker containers:
``` docker-compose up -d ```

Initialize db (schema and preseeded data)
``` docker exec -it challenge-app bash -c "npx prisma db push; npx prisma db seed;" ```

Seeder creates 2 roles:
- Admin
- User
2 Users with the given roles
and 5 parking spots for testing

The users will be displayed in the terminal. 
Copy the tokens and use them for API access.

As an example challenge.postman_collection.json postman collection is included.

The command swagger-autogen pregenerates swagger, but meta data wasn't included so the types and entities are not fully described.
``` npm swagger-autogen ```

## API

5 REST endponts.

- Create a new booking:
```
POST http://localhost:8000/api/v1/bookings
{
    "startDate": "2023-10-09T18:48:29Z",
    "endDate": "2023-10-10T18:48:29Z",
    "spotId": 1
}
```

- Update an existing booking:
```
PATCH http://localhost:8000/api/v1/bookings/4
{
    "startDate": "2023-10-09T18:48:29Z",
    "endDate": "2023-10-10T18:48:29Z",
    "spotId": 1
}
```

- View an existing booking:
```
GET http://localhost:8000/api/v1/bookings/4
```

- Get list of bookings
```
GET http://localhost:8000/api/v1/bookings
```

- Delete an existing booking
```
DELETE http://localhost:8000/api/v1/bookings/4
```


## Run tests

As an example there is only one tests available in the solution.

``` npm test ```




