import { Request } from "express";
import jsonwebtoken from 'jsonwebtoken'
import { PrismaClient } from '@prisma/client'
import { Role } from "./RoleRepository";

export const roleAdmin = 'Admin'
export const roleUser = 'User'

export type User = {
    id: number;
    email: string;
    fisrtName: string;
    lastName: string;
    token?: string;
    roleId?: number;
    role?: Role
}

export class UserRepository {
    private prisma: PrismaClient
    constructor(prisma: PrismaClient) {
        this.prisma = prisma
    }

    isAdmin(user: User) {
        return user?.role?.name === roleAdmin;
    }

    async getCurrentUser(req: Request): Promise<User|null>  {
        const token = req.headers.authorization as string
        const payload = jsonwebtoken.decode(token) as { email: string }

        return await this.getUserByEmail(payload.email)
    }

    async getUserByEmail(email: string): Promise<User|null> {
        try {
            const user = await this.prisma.user.findFirst({
                select: {
                    id: true,
                    fisrtName: true,
                    lastName: true, 
                    email: true,
                    role: true
                },
                where: {
                    email
                }
            })

            console.log('found user ', user)
            return user;
        } catch (e) {
            console.log(e)

            return null
        }
    }
}

export const NewUserRepository = () => {
    return new UserRepository(new PrismaClient());
}