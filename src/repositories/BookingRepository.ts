import { GetBookingsPaginatedDto } from "dto/GetBookingsPaginatedDto"
import { PrismaClient } from '@prisma/client'
import { BookingDto } from "dto/BookingDto"
import { User } from "./UserRepository"
import { Spot } from "./SpotRepository"
import { validate } from 'jsonschema'
import prisma from "../prisma"

export type Booking = {
    id: number 
    startDate: Date
    endDate: Date
    createdAt: Date
    updatedAt: Date
    userId?: number 
    spotId?: number
    spot?: Spot
    user?: User
}

export class BookingRepository {
    private prisma: PrismaClient
    constructor(prisma: PrismaClient) {
        this.prisma = prisma
    }

    isValidBookingPayload(data: unknown) {
        const result = validate(data, {
            "type": "object",
            "properties": {
                "startDate": {
                    "type": "string", "format": "date-time"
                },
                "endDate": {
                    "type": "string", "format": "date-time"
                },
                "spotId": {
                    "type": "number"
                }
            },
            "additionalProperties": false,
            "required": ["startDate", "endDate", "spotId"]
        })

        if (result.errors.length > 0) {
            return false;
        }

        const payload = data as BookingDto

        return new Date(payload.startDate) < new Date(payload.endDate)
    }

    async create(dto: BookingDto, userId: number): Promise<Booking|undefined> {
        const intersectingBooking = await this.prisma.booking.findFirst({
            where: {
                spotId: dto.spotId,
                startDate: {
                    lte: new Date(dto.endDate.toString()),
                  },
                endDate: {
                    gte: new Date(dto.startDate.toString())
                }
            }
        })

        if (intersectingBooking) {
            throw new Error('Intersecting booking already registered')
        }

        return await this.prisma.booking.create({
            data: {
                startDate: dto.startDate.toString(),
                endDate: dto.endDate.toString(),
                spotId: dto.spotId,
                userId,
                createdAt: new Date(),
                updatedAt: new Date()
            }
        })
    }

    async update(id: number, dto: BookingDto, userId?: number): Promise<Booking|undefined> {
        const intersectingBooking = await this.prisma.booking.findFirst({
            where: {
                spotId: dto.spotId,
                startDate: {
                    lte: new Date(dto.endDate.toString()),
                  },
                endDate: {
                    gte: new Date(dto.startDate.toString())
                },
                id: {
                    not: id
                }
            }
        })

        if (intersectingBooking) {
            throw new Error('Intersecting booking already registered')
        }

        return await this.prisma.booking.update({
            where: {
                id,
                userId
            },
            data: {
                startDate: dto.startDate.toString(),
                endDate: dto.endDate.toString(),
                spotId: dto.spotId,
                updatedAt: new Date()
            }
        })
    }

    async delete(id: number, userId?: number): Promise<boolean> {
        try {
            await this.prisma.booking.delete({
                select: {
                    startDate: true,
                    endDate: true,
                    createdAt: true,
                    updatedAt: true,
                    user: true,
                    spot: true
                },
                where: {
                    id, 
                    userId
                }
            })

            return true
        } catch (e) {
            return false
        } 
    }

    async get(id: number, userId?: number): Promise<Booking|null> {
        try {
            return await this.prisma.booking.findFirst({
                select: {
                    id: true,
                    startDate: true,
                    endDate: true,
                    createdAt: true,
                    updatedAt: true,
                    spot: true,
                    userId: true,
                },
                where: {
                    id, 
                    userId
                }
            })
        } catch (e) {
            return null
        } 
    }

    async getPaginated(params: GetBookingsPaginatedDto): Promise<{ pagination: { offset: number, limit: number, total: number }, items: Booking[]}> {
        const items = await this.prisma.booking.findMany({
            skip: params.offset,
            take: params.limit,
            select: {
                id: true,
                startDate: true,
                endDate: true,
                createdAt: true,
                updatedAt: true,
                spot: true,
                userId: true,
            },
            where: {
                userId: params.userId
            }
        })

        const total = await this.prisma.booking.count({
            where: {
                userId: params.userId
            }
        })

        return {
            pagination: {
                limit: params.limit,
                offset: params.offset,
                total
            },
            items,
        }
    }
}

export const NewBookingRepository = () => {
    return new BookingRepository(prisma)
}