import { NewBookingController } from "../controllers/BookingController";
import express, { Request, Response } from "express";
import auth from '../middlewares/auth'

const router = express.Router();

const bookingController = NewBookingController()

router.route('/bookings').get(auth, async (req: Request, res: Response) => {
    return await bookingController.index(req, res)
})

router.route('/bookings').post(auth, async (req: Request, res: Response) => {
    return await bookingController.create(req, res)
})

router.route('/bookings/:id').get(auth, async (req: Request, res: Response) => {
    return await bookingController.get(req, res)
})

router.route('/bookings/:id').delete(auth, async (req: Request, res: Response) => {
    return await bookingController.delete(req, res)
})

router.route('/bookings/:id').patch(auth, async (req: Request, res: Response) => {
    return await bookingController.edit(req, res)
})

export default router;