import { Response } from "express";

export const errorNotFound = (res: Response) => {
    res.status(404).json('Not found')
}

export const errorForbidden = (res: Response) => {
    res.status(401).json('Forbidden')
}

export const errorBadRequest = (res: Response, message: string = 'Bad Request') => {
    res.status(400).json(message)
}
