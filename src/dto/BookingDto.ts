export type BookingDto = {
    startDate: Date,
    endDate: Date,
    spotId: number
}