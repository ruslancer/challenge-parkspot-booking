export type GetBookingsPaginatedDto = {
    limit: number
    offset: number 
    userId?: number
}