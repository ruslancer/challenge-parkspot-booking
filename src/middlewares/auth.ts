import { errorForbidden } from "../errors"
import { Request, Response } from "express"
import jsonwebtoken from 'jsonwebtoken'
import { env } from 'process'

export default (req: Request, res: Response, next: Function) => {
    try {
        const token = req.headers.authorization as string

        if (!jsonwebtoken.verify(token, env.SECRET!)) {
            throw new Error('Wrong token')
        }

        next()
    } catch (error) {
        errorForbidden(res)
    }
}