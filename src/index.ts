'use strict';
 
import express from 'express'
import bodyParser from 'body-parser'
import apiRouter from './routes/api'

const PORT = 8000;
const HOST = '0.0.0.0';
const app = express();

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());

app.listen(PORT, HOST, () => {
  console.log(`Running on http://${HOST}:${PORT}`);
});

app.use("/api/v1", apiRouter);
