import { Request, Response } from "express";
import { BookingRepository, NewBookingRepository } from "../repositories/BookingRepository";
import { NewUserRepository, UserRepository } from "../repositories/UserRepository";
import { errorBadRequest, errorForbidden, errorNotFound } from "../errors";
import { BookingDto } from "dto/BookingDto";


export const defaultLimit = 10;

export class BookingController {
    private bookingRepository: BookingRepository
    private userRepository: UserRepository
    
    constructor(
        bookingRepository: BookingRepository,
        userRepository: UserRepository
    ) {
        this.bookingRepository = bookingRepository
        this.userRepository = userRepository
    }

    async index(req: Request, res: Response) {
        let limit: number = parseInt(req.query.limit as string)
        let offset: number = parseInt(req.query.offset as string)
        limit = isNaN(limit) ? defaultLimit : limit
        offset = isNaN(offset) ? 0 : offset

        const user = await this.userRepository.getCurrentUser(req);
        if (!user) {
            return errorForbidden(res)
        } 

        const filterByUserId = !this.userRepository.isAdmin(user) ? user.id : undefined
        const bookings = await this.bookingRepository.getPaginated({ limit, offset, userId: filterByUserId } )

        return res.status(200).json(bookings)
    }

    async get(req: Request, res: Response) {
        const id = parseInt(req.params.id)

        const user = await this.userRepository.getCurrentUser(req);
        if (!user) {
            return errorForbidden(res)
        } 

        const filterByUserId = !this.userRepository.isAdmin(user) ? user.id : undefined

        const booking = await this.bookingRepository.get(id, filterByUserId)
        if (!booking) {
            return errorNotFound(res)
        }

        res.status(200).json({ booking })
    }

    async create(req: Request, res: Response) {
        if (!this.bookingRepository.isValidBookingPayload(req.body)) {
            return errorBadRequest(res)
        }

        const user = await this.userRepository.getCurrentUser(req);
        if (!user) {
            return errorForbidden(res)
        } 

        try {
            const dto = req.body as BookingDto
            const booking = await this.bookingRepository.create(dto, user.id)

            res.status(200).json({ booking })
        } catch (error) {
            return errorBadRequest(res, (error as Error).message)
        }
    }

    async edit(req: Request, res: Response) {
        if (!this.bookingRepository.isValidBookingPayload(req.body)) {
            return errorBadRequest(res)
        }

        const id = parseInt(req.params.id)

        const user = await this.userRepository.getCurrentUser(req);
        if (!user) {
            return errorForbidden(res)
        } 

        const filterByUserId = !this.userRepository.isAdmin(user) ? user.id : undefined

        const booking = await this.bookingRepository.get(id, filterByUserId)
        if (!booking) {
            return errorNotFound(res)
        }

        const dto = req.body as BookingDto

        try {
            const updatedBooking = await this.bookingRepository.update(id, dto, filterByUserId)

            res.status(200).json({ booking: updatedBooking })
        } catch (error) {
            return errorBadRequest(res, (error as Error).message)
        }
    }

    async delete(req: Request, res: Response) {
        const id = parseInt(req.params.id)

        const user = await this.userRepository.getCurrentUser(req);
        if (!user) {
            return errorForbidden(res)
        } 

        const filterByUserId = !this.userRepository.isAdmin(user) ? user.id : undefined

        const booking = await this.bookingRepository.get(id, filterByUserId)
        if (!booking) {
            return errorNotFound(res)
        }

        await this.bookingRepository.delete(id, filterByUserId)

        res.status(200).json('Success')
    }
}

export const NewBookingController = () => {
    return new BookingController(
        NewBookingRepository(),
        NewUserRepository(),
    );
}